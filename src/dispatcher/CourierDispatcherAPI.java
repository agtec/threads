package dispatcher;

public interface CourierDispatcherAPI {

    void sendCourier(Package aPackage);
    void packageDelivered(Courier courier);
}
