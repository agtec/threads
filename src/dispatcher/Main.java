package dispatcher;

public class Main {

    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher();

        Courier courier1 = new Courier(dispatcher, 1);
        Courier courier2 = new Courier(dispatcher, 2);
        Courier courier3 = new Courier(dispatcher, 3);

        dispatcher.addCourier(courier1);
        dispatcher.addCourier(courier2);
        dispatcher.addCourier(courier3);

        Package package1 = new Package(1);
        Package package2 = new Package(2);
        Package package3 = new Package(3);
        Package package4 = new Package(4);
        Package package5 = new Package(5);

        dispatcher.sendCourier(package1);
        dispatcher.sendCourier(package2);
        dispatcher.sendCourier(package3);
        dispatcher.sendCourier(package4);
        dispatcher.sendCourier(package5);
    }
}
