package dispatcher;

public class Courier implements Runnable {

    private int id;
    private Package aPackage;
    private Dispatcher dispatcher;

    public Courier(Dispatcher dispatcher, int id) {
        this.dispatcher = dispatcher;
        this.id = id;
    }

    public void startDeliveringPackage(Package aPackage) {
        this.aPackage = aPackage;
        new Thread(this).start();
    }

    @Override
    public void run() {
        System.out.printf("Package with id %s is in delivery by courier id %s \n", aPackage.getId(), id);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Package with id %s delivered by courier of id %s\n", aPackage.getId(), id);
        dispatcher.packageDelivered(this);
    }
}
