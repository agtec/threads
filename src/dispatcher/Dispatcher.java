package dispatcher;

import java.util.ArrayDeque;
import java.util.Queue;

public class Dispatcher implements CourierDispatcherAPI {

    private Queue<Courier> couriers = new ArrayDeque<>();

    @Override
    synchronized public void sendCourier(Package aPackage) {
        if (couriers.isEmpty()) {
            try {
                wait();
                Courier courier = couriers.poll();
                courier.startDeliveringPackage(aPackage);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        } else {
            Courier courier = couriers.poll();
            courier.startDeliveringPackage(aPackage);
        }
    }

    @Override
    synchronized public void packageDelivered(Courier courier) {
        couriers.add(courier);
        notify();
    }

    public Courier addCourier(Courier courier) {
        couriers.add(courier);
        return courier;
    }

}
